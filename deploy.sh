#!/bin/sh
set -e

docker-compose build --pull
docker-compose down
docker-compose up -d db

if [[ $DEPLOYMENT_WITH_PROD_DATA = "true" ]]
then
  docker build -t copy_prod_data ./copy_prod_data
  docker run --rm --link guestbook_prod_db:prod --link guestbook_${DEPLOYMENT_NAME}_db:target \
    --net db copy_prod_data
fi

docker-compose up -d --force-recreate guestbook