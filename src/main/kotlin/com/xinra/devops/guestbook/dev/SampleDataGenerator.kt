package com.xinra.devops.guestbook.dev

import com.xinra.devops.guestbook.entity.GuestbookEntry
import com.xinra.devops.guestbook.entity.GuestbookEntryRepository
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Profile("dev")
@Component
class SampleDataGenerator(private val guestbookEntryRepository: GuestbookEntryRepository) {

  @PostConstruct
  private fun generateSampleData() {
    if (guestbookEntryRepository.count() > 0) {
      return
    }
    guestbookEntryRepository.save(GuestbookEntry("This is a sample entry for development"))
    guestbookEntryRepository.save(GuestbookEntry("This is another sample entry for development"))
  }
}
