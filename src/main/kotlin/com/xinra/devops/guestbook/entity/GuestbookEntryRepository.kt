package com.xinra.devops.guestbook.entity

import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface GuestbookEntryRepository : CrudRepository<GuestbookEntry, UUID>
