package com.xinra.devops.guestbook.entity

import java.time.Instant
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class GuestbookEntry(var content: String) {
  @Id
  val id = UUID.randomUUID()
  val createdAt = Instant.now()
}
