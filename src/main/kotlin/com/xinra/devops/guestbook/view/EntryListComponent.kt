package com.xinra.devops.guestbook.view

import com.vaadin.flow.component.html.H3
import com.vaadin.flow.component.html.Paragraph
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.xinra.devops.guestbook.entity.GuestbookEntry
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Locale


class EntryListComponent(entries: Iterable<GuestbookEntry>) : VerticalLayout() {

  init {
    isPadding = false
    setEntries(entries)
  }

  fun setEntries(entries: Iterable<GuestbookEntry>) {
    removeAll()
    entries.sortedByDescending { it.createdAt }.map { EntryComponent(it) }.forEach { add(it) }
  }

  private class EntryComponent(entry: GuestbookEntry) : VerticalLayout() {
    var formatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
        .withLocale(Locale.UK)
        .withZone(ZoneId.systemDefault())

    init {
      addClassName("guestbook-entry")
      add(H3("By Anonymous at " + formatter.format(entry.createdAt)))
      add(Paragraph(entry.content))
    }
  }
}
