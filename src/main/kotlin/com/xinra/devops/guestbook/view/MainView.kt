package com.xinra.devops.guestbook.view

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.dependency.StyleSheet
import com.vaadin.flow.component.html.H1
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.textfield.TextArea
import com.vaadin.flow.data.value.ValueChangeMode
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.server.PWA
import com.xinra.devops.guestbook.entity.GuestbookEntry
import com.xinra.devops.guestbook.entity.GuestbookEntryRepository

@Route
@PWA(name = "DevOps Guestbook", shortName = "Guestbook")
@StyleSheet("style.css")
@PageTitle("DevOps Guestbook")
class MainView(private val guestbookEntryRepository: GuestbookEntryRepository) : VerticalLayout() {

  private val entryListComponent = EntryListComponent(guestbookEntryRepository.findAll())

  init {
    addClassName("container")
    add(H1("DevOps Guestbook"))
    val textArea = TextArea("New Entry")
    textArea.setWidthFull()
    textArea.height = "100px"
    textArea.valueChangeMode = ValueChangeMode.EAGER
    add(textArea)
    add(Button("Submit") {
      if (!textArea.value.isNullOrEmpty()) {
        createNewEntry(textArea.value)
        textArea.value = ""
        Notification.show("✅ Entry created", 3000, Notification.Position.TOP_CENTER)
      }
    })
    add(entryListComponent)
  }

  private fun createNewEntry(content: String) {
    guestbookEntryRepository.save(GuestbookEntry(content))
    updateList()
  }

  private fun updateList() {
    entryListComponent.setEntries(guestbookEntryRepository.findAll())
  }
}
