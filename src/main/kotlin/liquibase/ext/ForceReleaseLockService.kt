package liquibase.ext

import liquibase.exception.DatabaseException
import liquibase.exception.LockException
import liquibase.lockservice.StandardLockService

// NOT FOR PRODUCTION
// https://stackoverflow.com/a/55525305/5519485
class ForceReleaseLockService : StandardLockService() {
  override fun getPriority(): Int {
    return super.getPriority() + 1
  }

  @Throws(LockException::class)
  override fun waitForLock() {
    try {
      super.forceReleaseLock()
    } catch (e: DatabaseException) {
      throw LockException("Could not enforce getting the lock.", e)
    }
    super.waitForLock()
  }
}
