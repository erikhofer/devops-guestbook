#!/bin/sh
set -e

echo "Dumping prod data"
PGHOST=prod PGUSER=postgres PGPASSWORD=postgres pg_dump guestbook > /tmp/dump.sql

echo "Resetting database"
PGHOST=target PGUSER=postgres PGPASSWORD=postgres dropdb guestbook
PGHOST=target PGUSER=postgres PGPASSWORD=postgres createdb guestbook

echo "Importing prod data"
PGHOST=target PGUSER=postgres PGPASSWORD=postgres psql guestbook < /tmp/dump.sql