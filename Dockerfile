FROM openjdk:8-alpine

EXPOSE 8080

ADD build/libs/guestbook-*.jar /app/guestbook.jar

WORKDIR /app

CMD ["java", "-jar", "guestbook.jar"]